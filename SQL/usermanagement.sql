create table t_user (id SERIAL PRIMARY KEY,name VARCHAR(256) not null,login_id VARCHAR(256) not null,
					login_password VARCHAR(256) not null,create_date date not NULL);
//---------------------------------------------------------------------------------------------------//
create table m_item (id serial PRIMARY KEY, name VARCHAR(256) not null, detail text not null,
					price int not null,file_name VARCHAR(256) not null, category_id int not null,
					seller_id int not null);
//---------------------------------------------------------------------------------------------------//
create table category(id serial PRIMARY KEY,name VARCHARACTER(256) not null);
//---------------------------------------------------------------------------------------------------//
create table message(id serial PRIMARY KEY,item_id int not null,user_id int not null,message text not 						null,create_date date not null);
//---------------------------------------------------------------------------------------------------//